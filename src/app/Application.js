import config from '../config';
import EventEmitter from 'eventemitter3';

const EVENTS = {
  APP_READY: 'app_ready',
};

/**
 * App entry point.
 * All configurations are described in src/config.js
 */
export default class Application extends EventEmitter {
  constructor() {
    super();

    this.config = config;
    this.data = {
      count: 0,
      planets: 0,
    };

    this.getData();
  }

  static get events() {
    return EVENTS;
  }

  /**
   * Initializes the app.
   * Called when the DOM has loaded. You can initiate your custom classes here
   * and manipulate the DOM tree. Task data should be assigned to Application.data.
   * The APP_READY event should be emitted at the end of this method.
   */

   async getApi(url) {
    const res = await fetch(url);
    const data = await res.json();

    return data;
  }
  async getData() {
    const baseUrl = 'https://swapi.boom.dev/api/planets/';

    const response = await this.getApi(baseUrl);

    const planets = [];

    this.data.count = response.count;
    for (let index = 1; index <= this.data.count; index++) {
      const planet = await this.getApi(baseUrl + index);

      planets.push(planet);
    }
    this.data.planets = planets;
    this.init();


    /**  
     * console.log(planets); 
     */
  }
  async init() {
    // Initiate classes and wait for async operations here.

    this.emit(Application.events.APP_READY);
  }
}

